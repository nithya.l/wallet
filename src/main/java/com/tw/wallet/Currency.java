package com.tw.wallet;

public enum Currency {
    RUPEE(1),
    DOLLAR(75);

    private final double baseFactor;

    Currency(double baseFactor) {
        this.baseFactor = baseFactor;
    }

    public double getBaseFactor() {
        return this.baseFactor;
    }
}
