package com.tw.wallet;

import com.tw.wallet.exception.InsufficientBalanceException;

public class Wallet {
    private Money balance;

    public Wallet() {
        this.balance = new Money(0.0, Currency.RUPEE);
    }

    public void put(Money money) {
        balance = balance.add(money);
    }

    public void take(Money money) throws InsufficientBalanceException {
        if (balance.isLessThan(money)) {
            throw new InsufficientBalanceException();
        }

        balance = balance.debit(money);
    }

    public Money getBalanceInSpecificCurrency(Currency currency) {
        double amount = balance.convertTo(currency);
        return new Money(amount,currency);
    }


}
