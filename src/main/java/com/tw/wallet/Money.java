package com.tw.wallet;

import java.util.Objects;

public class Money {
    private final double amount;
    private final Currency currency;

    public Money(double amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }


    public Money add(Money otherMoney) {

        double convertedMoney = otherMoney.convertTo(this.currency);
        return new Money((this.amount + convertedMoney), this.currency);
    }


    public Money debit(Money otherMoney) {
        double convertedMoney = otherMoney.convertTo(this.currency);
        return new Money((amount - convertedMoney), this.currency);
    }

    public boolean isLessThan(Money otherMoney) {
        double anotherMoney = otherMoney.convertTo(this.currency);
        return (amount - anotherMoney) < 0;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;
        return Double.compare(money.convertToBaseFactor(), this.convertToBaseFactor()) == 0;

    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }


    public double convertToBaseFactor() {
        return amount * this.currency.getBaseFactor();
    }

    public double convertTo(Currency currency) {
        return (amount * this.currency.getBaseFactor()) / currency.getBaseFactor();
    }
}
