package com.tw.wallet;

import com.tw.wallet.Currency;
import com.tw.wallet.exception.InsufficientBalanceException;
import com.tw.wallet.Money;
import com.tw.wallet.Wallet;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class WalletTest {

    @Test
    void shouldBeAbleToAddMoneyToWallet() {
        Wallet wallet = new Wallet();
        Money oneRupee = new Money(1, Currency.RUPEE);

        wallet.put(oneRupee);

        assertThat(wallet.getBalanceInSpecificCurrency(Currency.RUPEE), is(equalTo(new Money(1, Currency.RUPEE))));

    }

    @Test
    void shouldBeAbleToRetrieveMoneyFromWalletWhenItIsExplicitlyAdded() throws InsufficientBalanceException {
        Wallet wallet = new Wallet();
        Money twoRupee = new Money(2, Currency.RUPEE);
        wallet.put(twoRupee);
        Money oneRupee = new Money(1, Currency.RUPEE);

        wallet.take(oneRupee);

        assertThat(wallet.getBalanceInSpecificCurrency(Currency.RUPEE), is(equalTo(new Money(1, Currency.RUPEE))));
    }

    @Test
    void shouldBeAbleToRetrieveMoneyFromWalletWhenItIsNotExplicitlyAdded() throws InsufficientBalanceException {
        Wallet wallet = new Wallet();
        Money oneDollar = new Money(1, Currency.DOLLAR);
        wallet.put(oneDollar);
        Money tenRupee = new Money(10, Currency.RUPEE);

        wallet.take(tenRupee);

        assertThat(wallet.getBalanceInSpecificCurrency(Currency.RUPEE), is(equalTo(new Money(65, Currency.RUPEE))));
    }

    @Test
    void shouldNotBeAbleToRetrieveMoneyFromWalletWhenThereIsNotEnoughWalletBalance() {
        Wallet wallet = new Wallet();
        Money oneRupee = new Money(1, Currency.RUPEE);
        wallet.put(oneRupee);
        Money tenRupee = new Money(10, Currency.RUPEE);

        assertThrows(InsufficientBalanceException.class, () -> wallet.take(tenRupee));
    }

    @Test
    void shouldGiveTotalMoneyInRupeesWhenPreferredCurrencyIsRupees() {
        Wallet wallet = new Wallet();
        wallet.put(new Money(1, Currency.DOLLAR));

        assertThat(wallet.getBalanceInSpecificCurrency(Currency.RUPEE), is(equalTo(new Money(75, Currency.RUPEE))));
    }

    @Test
    void shouldGiveTotalMoneyInDollarsWhenPreferredCurrencyIsDollars() {
        Wallet wallet = new Wallet();
        wallet.put(new Money(150, Currency.RUPEE));

        assertThat(wallet.getBalanceInSpecificCurrency(Currency.DOLLAR), is(equalTo(new Money(2, Currency.DOLLAR))));
    }


}
