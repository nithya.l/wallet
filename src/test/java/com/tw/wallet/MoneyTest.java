package com.tw.wallet;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

class MoneyTest {

    @Test
    void shouldReturnTwoRupeesWhenOneRupeeIsAddedWithOneRupee() {
        Money oneRupee = new Money(1, Currency.RUPEE);
        Money anotherOneRupee = new Money(1, Currency.RUPEE);

        Money result = oneRupee.add(anotherOneRupee);

        assertThat(result, is(equalTo(new Money(2, Currency.RUPEE))));

    }

    @Test
    void shouldReturnOneRupeeWhenOneRupeeIsSubtractedFromTwoRupees() {
        Money oneRupee = new Money(1, Currency.RUPEE);
        Money twoRupee = new Money(2, Currency.RUPEE);

        Money result = twoRupee.debit(oneRupee);

        assertEquals(result, oneRupee);
    }


    @Test
    void shouldConvertMoneyInPreferredCurrency() {
        Money seventyFiveRupee = new Money(75, Currency.RUPEE);

        double result = seventyFiveRupee.convertTo(Currency.DOLLAR);
        Money actualMoney = new Money(result, Currency.DOLLAR);

        assertThat(actualMoney, is(equalTo(new Money(1, Currency.DOLLAR))));
    }

}